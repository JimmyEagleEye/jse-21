package ru.korkmasov.tsc.exeption.entity;


import ru.korkmasov.tsc.exeption.AbstractException;

public class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Error. Task not found.");
    }

}
