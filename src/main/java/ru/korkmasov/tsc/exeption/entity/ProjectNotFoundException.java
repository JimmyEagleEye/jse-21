package ru.korkmasov.tsc.exeption.entity;

import ru.korkmasov.tsc.exeption.AbstractException;

public class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error. Project not found.");
    }

}
