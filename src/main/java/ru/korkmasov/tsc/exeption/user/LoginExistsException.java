package ru.korkmasov.tsc.exeption.user;

import ru.korkmasov.tsc.exeption.AbstractException;

public class LoginExistsException extends AbstractException {

    public LoginExistsException() {
        super("Error! Login already exists...");
    }

}
