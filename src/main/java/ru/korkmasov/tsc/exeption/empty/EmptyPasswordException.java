package ru.korkmasov.tsc.exeption.empty;

import ru.korkmasov.tsc.exeption.AbstractException;

public class EmptyPasswordException extends AbstractException {

    public EmptyPasswordException() {
        super("Error. Password is empty...");
    }

}
