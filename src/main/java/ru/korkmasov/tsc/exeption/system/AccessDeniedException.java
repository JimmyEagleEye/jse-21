package ru.korkmasov.tsc.exeption.system;

import ru.korkmasov.tsc.exeption.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error. Access denied.");
    }

}
