package ru.korkmasov.tsc.repository;

import ru.korkmasov.tsc.api.repository.IUserRepository;
import ru.korkmasov.tsc.model.User;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findById(final String id) {
        return list.stream()
                .filter(e -> id.equals(e.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User findByLogin(final String login) {
        return list.stream()
                .filter(e -> login.equals(e.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User findByEmail(final String email) {
        return list.stream()
                .filter(e -> email.equals(e.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User removeUser(final User user) {
        list.remove(user);
        return user;
    }

    @Override
    public User removeUserById(final String id) {
        list.stream()
                .filter(e -> id.equals(e.getId()))
                .findFirst()
                .ifPresent(this::removeUser);
        return null;
    }

    @Override
    public User removeUserByLogin(final String login) {
        list.stream()
                .filter(e -> login.equals(e.getLogin()))
                .findFirst()
                .ifPresent(this::remove);
        return null;
    }

    @Override
    public boolean existsByLogin(final String login) {
        return list.stream()
                .anyMatch(e -> login.equals(e.getLogin()));
    }

    public boolean existsByEmail(final String email) {
        return list.stream()
                .anyMatch(e -> email.equals(e.getEmail()));
    }
}
