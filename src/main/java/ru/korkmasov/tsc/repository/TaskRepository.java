package ru.korkmasov.tsc.repository;

import ru.korkmasov.tsc.api.repository.ITaskRepository;
import ru.korkmasov.tsc.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findALLTaskByProjectId(final String userId, final String projectId) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()) && projectId.equals(e.getProjectId()))
                .collect(Collectors.toList());
    }

    @Override
    public List<Task> removeAllTaskByProjectId(final String projectId, final String userId) {
        list.stream()
                .filter(e -> userId.equals(e.getUserId()) && projectId.equals(e.getProjectId()))
                .forEach(this::remove);
        return null;
    }

    @Override
    public Task assignTaskByProjectId(final String userId, final String projectId, final String taskId) {
        final Task task = findOneById(userId, taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unassignTaskByProjectId(final String userId, final String taskId) {
        final Task task = findOneById(userId, taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

    @Override
    public void add(final String userId, Task task) {
        List<Task> list = findAll(userId);
        list.add(task);
    }

    @Override
    public void remove(final String userId, final Task task) {
        List<Task> list = findAll(userId);
        list.remove(task);
    }

    @Override
    public Task findOneById(final String userId, final String id) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()) && id.equals(e.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public Task removeOneById(final String userId, final String id) {
        list.stream()
                .filter(e -> userId.equals(e.getUserId()) && id.equals(e.getId()))
                .findFirst()
                .ifPresent(this::remove);
        return null;
    }

    @Override
    public Task findOneByIndex(final String userId, final Integer index) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .skip(index - 1)
                .findFirst()
                .orElse(null);
    }

    @Override
    public Task removeOneByIndex(final String userId, final Integer index) {
        list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .findFirst()
                .ifPresent(this::remove);
        return null;
    }

    @Override
    public Task findOneByName(final String userId, final String name) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()) && name.equals(e.getName()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public Task removeOneByName(final String userId, final String name) {
        list.stream()
                .filter(e -> userId.equals(e.getUserId()) && name.equals(e.getName()))
                .findFirst()
                .ifPresent(this::remove);
        return null;
    }

    @Override
    public void removeAllByProjectId(final String userId, final String projectId) {
        list.stream()
                .filter(e -> userId.equals(e.getUserId()) && projectId.equals(e.getProjectId()))
                .findFirst()
                .ifPresent(this::remove);
    }

    @Override
    public boolean existsByName(final String userId, final String name) {
        return list.stream()
                .anyMatch(e -> name.equals(e.getName()));
    }

    @Override
    public String getIdByIndex(int index) {
        return list.get(index).getId();
    }

}








